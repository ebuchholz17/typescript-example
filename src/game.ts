import * as Phaser from "phaser-ce";

import {PreloaderState} from "./states/PreloaderState";
import {GameState} from "./states/GameState";

export class Game {

    private _divName: string;

    public constructor (divName: string) {
        this._divName = divName;
    }

    public init (): void {
        let game = new Phaser.Game(720, 480, Phaser.AUTO, this._divName);
        game.state.add("PreloaderState", PreloaderState);
        game.state.add("GameState", GameState);
        game.state.start("PreloaderState");
    }
}
