export class PreloaderState extends Phaser.State {

    public preload (): void {
        this.load.image("image", "build/assets/baseball.png");
    }

    public create (): void {
        this.game.state.start("GameState");
    }

}
