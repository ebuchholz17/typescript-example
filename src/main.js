window.PIXI = require("pixi.js");
window.p2 = require("p2");
window.Phaser = require("phaser");

(function () {
    var Game = require("game").Game;
    var divName = "game";

    function initGame () {
        var game = new Game(divName);
        game.init();
    }

    function waitForDiv () {
        if (document.querySelector("#" + divName) != null) {
            initGame();
        }
        else {
            setTimeout(waitForDiv, 500);
        }
    }

    waitForDiv();
})();
