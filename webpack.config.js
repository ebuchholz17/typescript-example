var CopyWebpackPlugin = require("copy-webpack-plugin");
var path = require("path");
var webpack = require("webpack");

var phaserModule = path.join(__dirname, '/node_modules/phaser-ce/');
var phaser = path.join(phaserModule, 'build/custom/phaser-split.js'),
  pixi = path.join(phaserModule, 'build/custom/pixi.js'),
  p2 = path.join(phaserModule, 'build/custom/p2.js');

module.exports = {
    entry: "main",
    output: {
        filename: "build/typescript_example.js"
    },
    module: {
        rules: [
            { test: /\.ts$/, loader: "ts-loader" }
        ]
    },
    plugins: [
        //new webpack.optimize.UglifyJsPlugin({
        //    compress: true,
        //    comments: false,
        //    sourceMap: true
        //}),
        new CopyWebpackPlugin([
            {
                from: "src/assets",
                to: "build/assets"
            }
        ])
    ],
    resolve: {
        alias: {
            "phaser": phaser,
            "pixi.js": pixi,
            "p2": p2,
        },
        extensions: [
            ".js",
            ".ts"
        ],
        modules: [
            "src",
            "node_modules"
        ]
    },
    devtool: "source-map"
};
